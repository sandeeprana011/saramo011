package com.znsoftech.znsoftechlaundry;

/**
 * Created by sandeeprana on 16/12/15.
 */
public class FlagLaun {
   public static final String cCITY = "city";
   public static final String cCOUNTRY = "country";
   public static final String cLaundryCity = "LaundryCity";
   public static final String cSTATUS = "status";
   public static final String cSTATUS_MESSAGE = "stauts_message";
   public static String cDATA = "data";
   public static String cBANNER_ID = "BannerID";
   public static String cLAUNDRY_ID = "LaundryID";
   public static String cLAUNDRY_NAME = "LaundryName";
   public static String cDEALTITLE = "DealTitle";
   public static String cDEALID = "DealID";
   public static String cDEALTEXT = "DealText";
   public static String cDEALIMAGEURL = "DealImage";
   public static String cLAUNDRYADDRESS = "LaundryAddress";

   public static String cLAUNDRYLATITUDE = "LaundryLat";
   public static String cLAUNDRYLONGITUDE = "LaundryLong";
   public static String cRANK = "Rank";
   public static String DAta_STATUS = "Status";
   public static String cLAUNDRYSERVICEAREA = "LaundryServiceArea";
   public static String cAVG_RATING = "avgratings";
}
